const names = ['ФИО: ', 'Курс: ', 'Факультет: ', 'Номер группы: ', 'Предмет: ', 'Преподаватель: ', 'Дата и время: ', 'Форма сдачи: '];

function check_data(){
    create_table();
    fill_table();
    const body = document.body;
    const button = document.createElement("button");
    button.id = "confirm";
    button.textContent ="Подтвердить";
    button.onclick=Res;
    document.body.appendChild(button);
    document.getElementById("btn_reg").disabled = true;
}

function Res() {
    document.location.reload();
}

function create_table(){
    const body = document.body;
    tbl = document.createElement('table');
    tbl.style.border = '1px solid saddlebrown';

    for (let i = 0; i < 9; i++) {
        const tr = tbl.insertRow();
        for (let j = 0; j < 2; j++) {
            const td = tr.insertCell();
            if (i === 0 && j === 1) {
                break;
            } 
            else {
                td.appendChild(document.createTextNode(`Cell I${i}/J${j}`));
                td.style.border = '1px solid saddlebrown';
                if (i === 0 && j === 0) {
                    td.setAttribute('colSpan', '2');
                    td.innerHTML = "ПРОВЕРЬТЕ И ПОДТВЕРДИТЕ ЗАПИСЬ НА ЗАЧЕТ";
                    td.style.fontWeight = "bold";
                }
            }
        }
    }
    body.appendChild(tbl);
}

function fill_table(){
    var table = document.getElementsByTagName('table')[0];
    var td = table.getElementsByTagName('td');
    let j = 0;

    for (let i = 2; i < 18; i+=2) { 
        if(i % 2 == 0){
            td[i].textContent = names[j];
            j++
        }
    }

    j = 3;

    td[3].textContent = document.getElementsByClassName("data")[1].value + ' ' + document.getElementsByClassName("data")[0].value + ' ' + document.getElementsByClassName("data")[2].value;
    td[15].textContent = document.getElementsByClassName("data")[8].value + " " + document.getElementsByClassName("data")[9].value;
    for (let i = 5; i < 18; i+=2) { 
        if (i === 15) { 
            j++;
        }
        else {
            td[i].textContent = document.getElementsByClassName("data")[j].value;
            j++;
        }
    }

    let options = document.getElementsByClassName('data');
    let option_value;
    for (let j = 0; options.length; j++){
        if(options[j].checked){
            option_value = options[j].value;
            break;
        }
    }
    td[17].textContent = option_value;
}